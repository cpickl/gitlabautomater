import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

repositories {
    mavenCentral()
}

val applicationVersion = "2"

plugins {
    kotlin("jvm") version "1.7.21"
    kotlin("plugin.serialization") version "1.7.21"
    id("application")
    id("org.openjfx.javafxplugin") version "0.0.13"
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("com.github.ben-manes.versions") version "0.43.0"
}

dependencies {
    implementation("no.tornado:tornadofx:1.7.20")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-javafx:1.6.4")

    fun ktor(artifact: String) = "io.ktor:ktor-$artifact:2.1.3"
    listOf(
        "client-core",
        "client-cio",
        "client-logging",
        "client-content-negotiation",
        "serialization-kotlinx-json"
    ).forEach {
        implementation(ktor(it))
    }
}

javafx {
    version = "19"
    modules = listOf("javafx.controls", "javafx.fxml")
}

application {
    mainClass.set("com.cm.cpickl.devautomator.gui.DevAutomatorGuiApp")
}

tasks.register<Copy>("injectProperties") {
    from("src/main/resources") {
        include("about.properties")
        destinationDir = File("build/resources/main")
        duplicatesStrategy = DuplicatesStrategy.INCLUDE
        filter(org.apache.tools.ant.filters.ReplaceTokens::class, "tokens" to mapOf("version" to applicationVersion))
    }
}
tasks.named("processResources").get().finalizedBy("injectProperties")

tasks {
    named<ShadowJar>("shadowJar") {
        archiveBaseName.set("shadow")
    }
}

tasks {
    build {
        dependsOn(shadowJar)
    }
}

tasks.register<Copy>("copyDistribution") {
    from("build/libs") {
        include("**/shadow-all.jar")
    }
    into("build/distributions/")
    rename {
        require(it == "shadow-all.jar")
        "DevAutomator.jar"
    }
}

tasks.named("shadowJar").get().finalizedBy("copyDistribution")

tasks.withType<com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask> {
    val rejectPatterns =
        listOf("""^.*-ea\+[\d]+$""").map { Regex(it) }
    rejectVersionIf {
        val version = candidate.version.toLowerCase()
        rejectPatterns.any {
            it.matches(version)
        }
    }
}
