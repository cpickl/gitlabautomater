package com.cm.cpickl.devautomator

import java.io.File

object Projects {

    private val cmWorkspace = File("/Users/christoph.pickl/workspace/cm")

    val borealis = Project(
        displayName = "Borealis SDK",
        projectId = 41543994,
        epicId = null,
        localFolder = File(cmWorkspace, "/borealis_/borealis-sdk"),
    )
    val authorizationPortal = Project(
        displayName = "Authorization-Portal",
        projectId = 39750314,
        epicId = 613555,
        localFolder = File(cmWorkspace, "/authorization-portal"),
    )
    val auroraSDK = Project(
        displayName = "Aurora SDK",
        projectId = 44459835,
        epicId = null,
        localFolder = File(cmWorkspace, "/aurora-sdk"),
    )
    val auroraExamples = Project(
        displayName = "Aurora Examples",
        projectId = 44459868,
        epicId = null,
        localFolder = File(cmWorkspace, "/aurora-examples"),
    )
    val auroraBackend = Project(
        displayName = "Aurora Backend",
        projectId = 44494099,
        epicId = null,
        localFolder = File("/Users/cpickl/workspace/cm/aurora-backend-demo"),
    )

    val all = listOf(authorizationPortal, auroraSDK, auroraBackend, auroraExamples, borealis)
    val default = auroraSDK
}

data class Project(
    val displayName: String,
    val projectId: Int,
    val epicId: Int?,
    val localFolder: File,
)
