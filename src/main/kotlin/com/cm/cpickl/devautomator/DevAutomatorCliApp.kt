package com.cm.cpickl.devautomator

import com.cm.cpickl.devautomator.core.Config
import com.cm.cpickl.devautomator.core.DevAutomator
import com.cm.cpickl.devautomator.core.StatusListener
import kotlinx.coroutines.runBlocking

object DevAutomatorCliApp : StatusListener {

    @JvmStatic
    fun main(args: Array<String>): Unit = runBlocking {
        println("Welcome to DEV automator for authorization-portal.")
        println()
        println("Enter the title of your work item:")
        print(">> ")
        val title = readln()
        println()
        DevAutomator(DevAutomatorCliApp).automate(Config.byCurrentUser(), title)
    }

    override fun onStatusChanged(statusMessage: String) {
        println("STATUS: $statusMessage")
    }
}
