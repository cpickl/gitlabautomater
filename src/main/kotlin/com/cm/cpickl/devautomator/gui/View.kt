package com.cm.cpickl.devautomator.gui

import com.cm.cpickl.devautomator.About
import com.cm.cpickl.devautomator.Project
import com.cm.cpickl.devautomator.Projects
import com.cm.cpickl.devautomator.core.Config
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.geometry.Pos
import javafx.scene.control.Alert
import javafx.scene.control.ButtonType
import javafx.scene.control.ListCell
import tornadofx.Field
import tornadofx.View
import tornadofx.action
import tornadofx.addClass
import tornadofx.alert
import tornadofx.button
import tornadofx.combobox
import tornadofx.enableWhen
import tornadofx.field
import tornadofx.fieldset
import tornadofx.filterInput
import tornadofx.form
import tornadofx.hbox
import tornadofx.isInt
import tornadofx.label
import tornadofx.selectedItem
import tornadofx.textfield
import tornadofx.vbox
import java.io.File

// help: https://docs.tornadofx.io/0_subsection/4_basic_controls

class RootView : View() {

    private val controller: MainController by inject()

    init {
        title = "Dev-Automator v${About.data.version}" + (if (Config.dummyLogic) " - DUMMY" else "")
    }

    override val root = vbox {
        hbox {
            alignment = Pos.CENTER
            label("Dev-Automator") {
                addClass(Styles.title)
            }
        }
        form {
            fieldset {
                ConfigField.values().forEach { config ->
                    field(config.def.label) {
                        config.addToField(this)
                    }
                }
            }
        }
        hbox {
            alignment = Pos.CENTER
            button {
                text = "Run"
                enableWhen {
                    controller.idle
                }
                minWidth = 150.0
                action {
                    val enteredTitle = ConfigField.Title.def.property.value
                    if (enteredTitle == null || enteredTitle.trim().isEmpty()) {
                        alert(
                            Alert.AlertType.WARNING,
                            "Mandatory title is missing",
                            null,
                            ButtonType.OK,
                            owner = this@RootView.currentWindow
                        )
                    } else {
                        controller.execute(ConfigField.buildConfig(), enteredTitle)
                    }
                }
            }
        }
        hbox {
            label("Status: ") {
                addClass(Styles.statusMessage)
            }
            label(controller.statusMessage) {
                addClass(Styles.statusMessage)
            }
        }
    }
}

private val textFieldAdder: Field.(ConfigField) -> Unit = { config ->
    textfield(config.def.property) {
        if (config.def.isNumeric) {
            filterInput { it.controlNewText.isInt() }
        }
        if (config.def.minWidth != null) {
            minWidth = config.def.minWidth
        }
    }
}

class ProjectListCell : ListCell<Project>() {
    override fun updateItem(item: Project?, empty: Boolean) {
        super.updateItem(item, empty)
        text = item?.displayName
    }
}

private val fxProjects = FXCollections.observableArrayList(Projects.all)

enum class ConfigField(
    val def: FieldDefinition,
    private val valueAdder: Field.(ConfigField) -> Unit = textFieldAdder,
) {
    AccessToken(FieldDefinition("Access Token")),
    UserId(FieldDefinition("User ID", isNumeric = false)),
    ProjectTemplate(FieldDefinition("Project", isMemorizable = false), valueAdder = {
        combobox(values = fxProjects) {
            buttonCell = ProjectListCell()
            selectionModel.select(Projects.default)
            setCellFactory {
                ProjectListCell()
            }
            setOnAction {
                selectedItem?.let { selectedProject ->
                    com.cm.cpickl.devautomator.gui.ConfigField.ProjectId.def.property.value = selectedProject.projectId.toString()
                    com.cm.cpickl.devautomator.gui.ConfigField.EpicId.def.property.value = selectedProject.epicId?.toString()
                        ?: ""
                    com.cm.cpickl.devautomator.gui.ConfigField.CheckoutLocation.def.property.value = selectedProject.localFolder.toString()
                }
            }
        }
    }),
    ProjectId(FieldDefinition("Project ID", isNumeric = false)),
    EpicId(FieldDefinition("Epic ID", isNumeric = false)),
    CheckoutLocation(FieldDefinition("Local Folder", minWidth = 450.0)),
    Title(FieldDefinition("Title", isMemorizable = false));

    fun addToField(field: Field) {
        valueAdder.invoke(field, this)
    }

    companion object {
        fun buildConfig() = Config(
            accessToken = AccessToken.def.property.value,
            userId = UserId.def.property.value.toInt(),
            projectId = ProjectId.def.property.value.toInt(),
            epicId = EpicId.def.property.value.let { if (it.isEmpty()) null else it.toInt() },
            checkoutLocation = File(CheckoutLocation.def.property.value),
        )
    }
}

data class FieldDefinition(
    val label: String,
    val isNumeric: Boolean = false,
    val isMemorizable: Boolean = true,
    val minWidth: Double? = null,
) {
    val property = SimpleStringProperty()
}
