package com.cm.cpickl.devautomator.gui

import java.util.prefs.Preferences

object Memory {

    private val prefs = Preferences.userNodeForPackage(Memory::class.java)
    private const val prefix = "DevAutomator-v1-"

    fun lookup(key: String): String =
        prefs.get(prefix + key, "")

    fun store(key: String, value: String): Unit =
        prefs.put(prefix + key, value)
}
