package com.cm.cpickl.devautomator.gui

import javafx.stage.Stage
import tornadofx.App
import tornadofx.launch

object DevAutomatorGuiApp {
    @JvmStatic
    fun main(args: Array<String>) {
        println("Starting GUI...")
        launch<DevAutomatorTornadoFxApp>(args)
    }
}

class DevAutomatorTornadoFxApp : App(RootView::class, Styles::class) {
    override fun start(stage: Stage) {
        super.start(stage)
        loadMemoryFields()
        stage.setOnCloseRequest {
            storeMemoryFields()
        }
    }

    private fun loadMemoryFields() {
        ConfigField.values().filter { it.def.isMemorizable }.forEach {
            val storedValue = Memory.lookup(it.memoryKey)
            it.def.property.value = storedValue
        }
    }

    private fun storeMemoryFields() {
        ConfigField.values().filter { it.def.isMemorizable }.forEach {
            Memory.store(it.memoryKey, it.def.property.value)
        }
    }
}

private val ConfigField.memoryKey
    get() = when (this) {
        ConfigField.AccessToken -> "accessToken"
        ConfigField.UserId -> "userId"
        ConfigField.ProjectTemplate -> "projectTemplate"
        ConfigField.ProjectId -> "projectId"
        ConfigField.EpicId -> "epicId"
        ConfigField.CheckoutLocation -> "checkoutLocation"
        ConfigField.Title -> "title"
    }
