package com.cm.cpickl.devautomator.gui

import com.cm.cpickl.devautomator.core.Config
import com.cm.cpickl.devautomator.core.DevAutomator
import com.cm.cpickl.devautomator.core.StatusListener
import javafx.scene.control.Alert
import javafx.scene.control.ButtonType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import tornadofx.Controller
import tornadofx.alert
import tornadofx.getProperty
import tornadofx.property

private val mainScope = CoroutineScope(Dispatchers.Main)

class MainController : Controller(), StatusListener {

    private val idleText = "Idle ..."
    private var statusMessageProperty by property(idleText)
    var statusMessage = getProperty(MainController::statusMessageProperty)
    private var idleProperty by property(true)
    var idle = getProperty(MainController::idleProperty)

    fun execute(config: Config, title: String) {
        idleProperty = false
        mainScope.launch {
            try {
                if (Config.dummyLogic) {
                    onStatusChanged("dummy 1")
                    delay(1000)
                    onStatusChanged("dummy 2")
                    delay(1000)
                    onStatusChanged("dummy 3")
                } else {
                    DevAutomator(this@MainController).automate(config, title)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                alert(
                    type = Alert.AlertType.ERROR,
                    header = "Failed to automate branch creation!",
                    title = "Error",
                    content = "${e::class.simpleName}: ${e.message}",
                    owner = this@MainController.primaryStage,
                    buttons = arrayOf(ButtonType.CLOSE)
                )
            } finally {
                idleProperty = true
                statusMessageProperty = idleText
            }
        }
    }

    override fun onStatusChanged(statusMessage: String) {
        println("Status changed to: $statusMessage")
        statusMessageProperty = statusMessage
    }

}
