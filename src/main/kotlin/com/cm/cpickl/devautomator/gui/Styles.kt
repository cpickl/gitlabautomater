package com.cm.cpickl.devautomator.gui

import javafx.scene.Cursor
import javafx.scene.paint.Color
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.c
import tornadofx.cssclass
import tornadofx.pt
import tornadofx.px


class Styles : Stylesheet() {
    companion object {
        val title by cssclass()
        val statusMessage by cssclass()
        val lightBlue = c("#E7F1FD")
        val darkBlue = c("#3173E0")
    }

    init {
        s(label, textField) {
            fontSize = 14.pt
        }
//        val flat = mixin {
//            backgroundInsets += box(0.px)
//            borderColor += box(Color.DARKGRAY)
//        }
        Stylesheet.root {
            backgroundColor += Color.WHITE
            padding = box(10.px)
        }
        title {
            fontSize = 30.pt
            textFill = darkBlue
        }
        statusMessage {
            fontSize = 12.pt
        }
        button {
            backgroundColor += lightBlue
            textFill = darkBlue
            fontSize = 16.pt
        }
        button and hover {
            backgroundColor += darkBlue
            textFill = Color.WHITE
            cursor = Cursor.HAND
        }
    }
}
