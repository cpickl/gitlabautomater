package com.cm.cpickl.devautomator.core

/** safe to be used for branch name and stuff. only [azAZ09-].*/
@JvmInline
value class CleanString private constructor(val value: String) {
    companion object {
        fun cleanify(dirty: String): CleanString {
            val sb = StringBuilder()
            dirty.toCharArray().forEach { c ->
                when (c) {
                    ' ' -> sb.append('-')
                    '-',
                    '_',
                    in '0'..'9',
                    in 'a'..'z',
                    in 'A'..'Z' -> sb.append(c)
                }
            }
            return CleanString(sb.toString())
        }
    }

    override fun toString() = value
}
