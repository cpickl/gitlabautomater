package com.cm.cpickl.devautomator.core

import java.io.File

object ProcessExecutor {
    fun exec(cwd: File, vararg commands: String) {
        println()
        val debugCommand = "[${cwd.absolutePath}]>> ${commands.toList().joinToString(" ")}"
        println(debugCommand)

        val pb = ProcessBuilder(*commands)
        pb.directory(cwd)
        pb.redirectErrorStream(true)
        pb.redirectOutput(ProcessBuilder.Redirect.INHERIT)
        val status = pb.start().waitFor()
        if (status != 0) {
            throw ProcessorException("Process returned invalid status code [$status] while executing: $debugCommand")
        }
        println()
    }
}

class ProcessorException(message: String) : Exception(message)
