package com.cm.cpickl.devautomator.core

enum class OperatingSystem(val propertyMarker: String) {
    MacOS("mac"),
    Windows("windows");

    companion object {
        val current = System.getProperty("os.name").lowercase().let { osName ->
            println("check $osName")
            OperatingSystem.values().firstOrNull { osName.contains(it.propertyMarker) }
        }
        val isMacOs = current == MacOS
    }
}