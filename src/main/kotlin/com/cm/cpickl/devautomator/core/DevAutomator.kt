package com.cm.cpickl.devautomator.core

import com.cm.cpickl.devautomator.core.gitlab.GitLabApi
import java.io.File

interface StatusListener {
    fun onStatusChanged(statusMessage: String)
}

class DevAutomator(
    private val listener: StatusListener,
) {

    suspend fun automate(config: Config, title: String) {
        verifyConfigCheckoutLocationIsGitRepo(config)
        val gitLab = GitLabApi(
            accessToken = config.accessToken,
        )
        val mergeRequestUrl = gitLab.createIssueBranchAndMR(
            epicId = config.epicId,
            userId = config.userId,
            projectId = config.projectId,
            title = title,
        )
        checkoutBranchLocally(config.checkoutLocation, CleanString.cleanify(title))

        if (OperatingSystem.isMacOs) {
            ProcessExecutor.exec(config.checkoutLocation, "open", mergeRequestUrl)
        }
    }

    private fun verifyConfigCheckoutLocationIsGitRepo(config: Config) {
        try {
            ProcessExecutor.exec(config.checkoutLocation, "git", "status")
        } catch (e: ProcessorException) {
            throw DevAutomatorException(
                "Given location [${config.checkoutLocation.absolutePath}] is not a valid GIT repository!",
                e
            )
        }
    }

    private suspend fun GitLabApi.createIssueBranchAndMR(
        projectId: Int,
        userId: Int,
        epicId: Int?,
        title: String
    ): String {
        listener.onStatusChanged("Creating GitLab issue ...")
        val issue = createIssue(
            projectId = projectId,
            userId = userId,
            epicId = epicId,
            title = title,
        )
        println(issue)
        listener.onStatusChanged("Creating GitLab branch ...")
        createBranch(projectId, CleanString.cleanify(title))
        listener.onStatusChanged("Creating GitLab merge request ...")
        val mergeRequest = createMergeRequest(projectId, userId, issue, buildMergeRequestDescription(issue.iid))
        println(mergeRequest)

        println("✅✅✅ DONE ✅✅✅")
        return mergeRequest.web_url
    }

    private fun checkoutBranchLocally(cwd: File, branchName: CleanString) {
        listener.onStatusChanged("Checkout local branch ...")
        ProcessExecutor.exec(cwd, "git", "pull")
        ProcessExecutor.exec(cwd, "git", "checkout", branchName.value)
        ProcessExecutor.exec(cwd, "idea", ".")
    }

    private fun buildMergeRequestDescription(issueId: Int) = """# Summary
Closes #${issueId}.

# Conformity

- [ ] [Code review guidelines followed according to chapter 4 "Secure coding standards" of the Software Development Standard document](https://clubmessage.sharepoint.com/:b:/s/pp.POSGatewayBV/EYxC5w7OyjNEiQrn0xlSDn4BKvgvB8DFVHvZipKfrLfJIw?e=vwZb4T)"""
}

class DevAutomatorException(message: String, cause: Exception? = null) : Exception(message, cause)