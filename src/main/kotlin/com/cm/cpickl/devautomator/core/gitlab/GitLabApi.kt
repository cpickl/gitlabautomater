package com.cm.cpickl.devautomator.core.gitlab

import com.cm.cpickl.devautomator.core.CleanString
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.cio.CIO
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpStatusCode
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json

/** See: https://docs.gitlab.com/ee/api/api_resources.html */
class GitLabApi(
    /** personal, project, group */
    private val accessToken: String,
) {
    private val baseUrl = "https://gitlab.com/api/v4"
    private val client = HttpClient(CIO) {
        install(ContentNegotiation) {
            json(Json {
                isLenient = true
                allowSpecialFloatingPointValues = true
                allowStructuredMapKeys = true
                prettyPrint = true
                useArrayPolymorphism = false
                ignoreUnknownKeys = true
            })
        }
        expectSuccess = false
        defaultRequest {
            header("PRIVATE-TOKEN", accessToken)
        }
    }

    suspend fun createIssue(
        projectId: Int,
        userId: Int,
        title: String,
        epicId: Int? = null
    ): IssueCreateResponse {
        println("Creating issue: $title")
        val response = client.post("$baseUrl/projects/$projectId/issues") {
            jsonContent()
            setBody(
                IssueCreateRequest(
                    id = projectId,
                    title = title,
                    assignee_id = userId,
                    epic_id = epicId,
                )
            )
        }
        response.require(HttpStatusCode.Created)
        println()
        return response.body()
    }

    suspend fun createBranch(
        projectId: Int,
        branchName: CleanString,
    ) {
        println("Creating branch: '$branchName'")
        val response = client.post("$baseUrl/projects/$projectId/repository/branches") {
            jsonContent()
            setBody(
                BranchCreateRequest(
                    id = projectId,
                    branch = branchName.value,
                    ref = "main",
                )
            )
        }
        response.require(HttpStatusCode.Created)
        println()
    }

    suspend fun createMergeRequest(
        projectId: Int,
        userId: Int,
        issue: IssueCreateResponse,
        description: String,
    ): MergeRequestCreateResponse {
        println("Creating merge request.")
        val response = client.post("$baseUrl/projects/$projectId/merge_requests") {
            jsonContent()
            setBody(
                MergeRequestCreateRequest(
                    id = projectId,
                    source_branch = CleanString.cleanify(issue.title).value,
                    target_branch = "main",
                    title = "Draft: ${issue.title}",
                    assignee_id = userId,
                    description = description,
                )
            )
        }
        response.require(HttpStatusCode.Created)
        println()
        return response.body()
    }

    suspend fun getProject(projectId: Int) {
        val response = client.get("$baseUrl/projects/$projectId")
        println(response.status)
        println(response.bodyAsText())
    }

    suspend fun getIssues() {
        val response = client.get("$baseUrl/issues")
        println(response.status)
        println(response.bodyAsText())
    }

    suspend fun getMRs() {
        val response = client.get("$baseUrl/merge_requests")
        println(response.status)
        println(response.bodyAsText())
    }
}

private suspend fun HttpResponse.require(expected: HttpStatusCode) {
    if (status != expected) {
        println(bodyAsText())
        error("Invalid response status: $status")
    }
}

private fun HttpRequestBuilder.jsonContent() {
    header(HttpHeaders.ContentType, ContentType.Application.Json.toString())
}
