package com.cm.cpickl.devautomator.core.gitlab

import kotlinx.serialization.Serializable

@Serializable
data class IssueCreateRequest(
    val id: Int,
    val title: String,
    val assignee_id: Int? = null,
    val epic_id: Int? = null,
)

@Serializable
data class IssueCreateResponse(
    val id: Int,
    val iid: Int,
    val title: String,
)

@Serializable
data class MergeRequestCreateRequest(
    val id: Int,
    val source_branch: String,
    val target_branch: String,
    val title: String,
    val assignee_id: Int? = null,
    val description: String? = null
)

@Serializable
data class MergeRequestCreateResponse(
    val id: Int,
    val iid: Int,
    val web_url: String,
)

@Serializable
data class BranchCreateRequest(
    val id: Int,
    val branch: String,
    val ref: String,
)
