package com.cm.cpickl.devautomator.core

import com.cm.cpickl.devautomator.Projects
import java.io.File

data class Config(
    val accessToken: String,
    val userId: Int,
    val projectId: Int,
    val epicId: Int?,
    val checkoutLocation: File,
) {
    companion object {
        val dummyLogic = System.getProperty("dummyLogic", "false").toBooleanStrict()

        private val userIds = mapOf("cpickl" to 11986807)

        fun byCurrentUser() =
            Config(
                epicId = Projects.default.epicId,
                projectId = Projects.default.projectId,
                userId = System.getProperty("user.name").let {
                    userIds[it]
                        ?: error("Unknown user '$it'! Please configure yourself in: ${Config::class.qualifiedName}")
                },
                checkoutLocation = Projects.default.localFolder,
                accessToken = System.getProperty("accessToken")
            )
    }

    init {
        require(accessToken.isNotEmpty())
        require(userId > 0)
        require(projectId > 0)
        require(epicId == null || epicId > 0)
        require(checkoutLocation.exists() && checkoutLocation.isDirectory) {
            "Invalid checkout location: ${checkoutLocation.absolutePath}"
        }
    }
}
