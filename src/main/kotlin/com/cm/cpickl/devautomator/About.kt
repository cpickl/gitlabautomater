package com.cm.cpickl.devautomator

import java.util.Properties

object About {
    val data by lazy {
        val props = Properties().apply {
            load(About::class.java.getResourceAsStream("/about.properties"))
        }
        AboutData(
            version = props.getProperty("version")
        )
    }
}

data class AboutData(
    val version: String,
)
