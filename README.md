# DevAutomator

Comes with a CLI and a GUI.

Does the following for you:

1. Create a GitLab issue.
2. Create a GitLab branch.
3. Create a GitLab merge request.
4. Checkout the branch.
5. Open IDEA with the branch.
6. Open browser with the merge request.

## Todos

* when hit run, mark mandatory input fields red if invalid/empty with some explanatory message with it
* support "presets": when working on different topics, change necessaryfields accordingly
* application version (filter resource; display in CLI/UI)
* ! add logging
* refactor fields, not global, but view-specific; yet model enum backed
* support CLI args: --version, --cli-interactie, --cli-batch (requires --propert.xxxx), --verbose (log to stdout),
  --help, --rest-preferences
* use ViewModel, so view is passive and doesnt know of controller, and controller only manipulates model (how to
  register onButtonClick handlers then?!)
    * make controller a 1:1 reference to view, and delegate logic to a view-independent service
* ? trello integration? drop down of all available ansigned tasks which are in "Doing" column
* dropdown of available GitLab projects + refresh button next to it
  ? memorize last choice (maybe list most recent used on top)
